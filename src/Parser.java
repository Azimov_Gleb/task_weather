import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;

public class Parser {
    ArrayList<WeatherDay> parser() throws Exception {
        Wether wes = new Wether();

        String inputString1 = wes.sendGet();
        JsonParser parser = new JsonParser();
        JsonElement Object = parser.parse(inputString1);
        JsonElement query = Object.getAsJsonObject().get("query");
        JsonElement results = query.getAsJsonObject().get("results");
        JsonElement channel = results.getAsJsonObject().get("channel");

        //JsonElement location = channel.getAsJsonObject().get("location");
        //String cityName = location.getAsJsonObject().get("city").getAsString();

        JsonElement item = channel.getAsJsonObject().get("item");
        JsonElement forecast = item.getAsJsonObject().get("forecast");
        JsonArray forecast1 = forecast.getAsJsonArray();

        ArrayList<WeatherDay> weatherCity = new ArrayList<>();

        for (JsonElement i : forecast1) {
            int code = i.getAsJsonObject().get("code").getAsInt();
            String date = i.getAsJsonObject().get("date").getAsString();
            String day = i.getAsJsonObject().get("day").getAsString();
            double minTemperature = i.getAsJsonObject().get("low").getAsDouble();
            double maxTemperature = i.getAsJsonObject().get("high").getAsDouble();
            String weather = i.getAsJsonObject().get("text").getAsString();
            WeatherDay weatherDay = new WeatherDay(code, date, day, minTemperature, maxTemperature, weather);

            weatherCity.add(weatherDay);
        }

        //print result
        for (WeatherDay i : weatherCity) {
            System.out.println(i.code + " " + i.date + " " + i.day + " " + i.minTemperature + " " + i.maxTemperature + " " + i.weather + " " + "\n");
        }
        return weatherCity;
    }


}