import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class Local {
    static final Locale ru = new Locale("ru");

    ArrayList data = new ArrayList<WeatherDay>();

    public static String Loc_Eng(Locale locale, int code) throws FileNotFoundException {
        String weather1 = "";

        if (Locale.ENGLISH.equals(locale)
                || locale.equals(ru)) {
            Scanner read = new Scanner(new FileReader(locale.toString().toUpperCase() + ".txt"));
            while (read.hasNextLine()) {
                String s = read.nextLine();
                String[] mass = s.split("\t");
                int Key_code = Integer.parseInt(mass[0]);
                if (Key_code == code) {
                    weather1 = mass[1];
                }
            }
        } else {
            Scanner read = new Scanner(new FileReader("EN.txt"));
            while (read.hasNextLine()) {
                String s = read.nextLine();
                String[] mass = s.split("\t");
                int Key_code = Integer.parseInt(mass[0]);
                if (Key_code == code) {
                    weather1 = mass[1];


                }

            }
        }
        return weather1;
    }
}
