import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Locale;

public class Saver {

    private Locale locale;

    public Saver(Locale locale) {
        this.locale = locale;
    }

    public double convertTemp(double tempereture) {
        return (tempereture - 32) * 1.8;
    }


    public void saver(ArrayList<WeatherDay> storage) throws Exception {

        Local loc = new Local();

        FileWriter writer = new FileWriter("output.csv");
        for (WeatherDay i : storage) {
            String day = i.getDay();
            double min_T = convertTemp(i.getMinTemperature());
            double max_T = convertTemp(i.getMaxTemperature());
            String weter = loc.Loc_Eng(locale, i.getCode());


            StringBuffer s = new StringBuffer();
            s.append(day);
            s.append(";");
            s.append(min_T);
            s.append(";");
            s.append(max_T);
            s.append(";");
            s.append(weter);
            s.append(";");
            s.append("\n");


            writer.write(s.toString());
        }
        writer.flush();
        writer.close();
    }
}